import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="#">Hasan</a>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link" href="#">Expert</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Works</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Portofolio</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Contact</a>
                </li>
              </ul>
            </div>
          </nav>
        </header> 
        <div className="body">
          <div class="container">
            <div class="row content-body">
              <div class="col">
                <img width="500px" src="https://images3.alphacoders.com/918/91835.jpg"></img>
              </div>
              <div class="col">
              <div class="col-content"> 
                <h1>Hasan Mubarok</h1>
                <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"></input>
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
              <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
              </div>
            </div>
            </div>
          </div>
        </div>
    );
  }
}

export default App;
